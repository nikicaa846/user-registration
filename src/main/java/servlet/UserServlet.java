package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import service.HtmlPageGeneratorService;
import service.UserService;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UserService userService = new UserService();
	HtmlPageGeneratorService htmlGeneratorService = new HtmlPageGeneratorService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<User> users = userService.findAllUsers();
		String generatedPage = htmlGeneratorService.generatedUsersPage(users);
		PrintWriter out = response.getWriter();
		out.print(generatedPage);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String address = request.getParameter("address");
		String city = request.getParameter("city");
		String country = request.getParameter("country");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		User user = new User(firstName, lastName, address, city, country, email, password);

		PrintWriter out = response.getWriter();

		if (userService.addUser(user) == false) {
			out.print("<div>");
			out.print(" User with same email address exist in base!\r\n");
			out.print("</div>");
			out.print("<form action=\"/Project/users\" method=\"get\">");
			out.print("Back to users page: <input type=\"submit\" value=\"users\">");
			out.print("</form>");
			out.close();
		} else {
			response.setIntHeader("Refresh", 1);
		}
	}

}
