package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import service.HtmlPageGeneratorService;
import service.UserService;

public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UserService userService = new UserService();
	HtmlPageGeneratorService htmlGeneratorService = new HtmlPageGeneratorService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = userService.findUser(request.getParameter("userEmail"));
		userService.deleteUser(user);
		List<User> users = userService.findAllUsers();
		String generatedPage = htmlGeneratorService.generatedUsersPage(users);
		PrintWriter out = response.getWriter();
		out.print(generatedPage);
		out.close();
	}

}
