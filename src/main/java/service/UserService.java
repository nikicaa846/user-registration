package service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import model.User;

public class UserService {

	static final String PATH = "C:\\Users\\PC\\Desktop\\Nikolina\\DevLabs\\Project\\data.xml";
	
	private PrintService printService;

	public NodeList getNodeList(int item, NodeList data) {
		if (data.item(item).getNodeType() == Node.ELEMENT_NODE) {
			return data.item(item).getChildNodes();
		}
		return null;
	}

	public String getNameOfCountry(int item, NodeList continents) {
		String country = "";
		if (continents.item(item).getNodeType() == Node.ELEMENT_NODE) {
			Element countryEl = (Element) continents.item(item);
			country = countryEl.getAttribute("name");
		}
		return country;
	}

	public User getUserNode(int item, NodeList countries, String countryName) {
		if (countries.item(item).getNodeType() == Node.ELEMENT_NODE) {
			Element user = (Element) countries.item(item);
			String firstName = user.getElementsByTagName("first_name").item(0).getTextContent();
			String lastName = user.getElementsByTagName("last_name").item(0).getTextContent();
			String address = user.getElementsByTagName("address").item(0).getTextContent();
			String city = user.getElementsByTagName("city").item(0).getTextContent();
			String email = user.getElementsByTagName("email").item(0).getTextContent();
			String password = user.getElementsByTagName("password").item(0).getTextContent();
			return new User(firstName, lastName, address, city, countryName, email, password);
		}
		return null;
	}

	public List<User> findAllUsers() {
		List<User> users = new ArrayList<User>();
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = (Document) builder.parse(new File(PATH));
			((org.w3c.dom.Document) doc).normalizeDocument();

			Element root = (Element) doc.getDocumentElement();
			NodeList data = root.getChildNodes();

			for (int i = 0; i < data.getLength(); i++) {
				NodeList continents = getNodeList(i, data);
				if (continents != null) {
					for (int j = 0; j < continents.getLength(); j++) {
						NodeList countries = getNodeList(j, continents);
						if (countries != null) {
							String countryName = getNameOfCountry(j, continents);
							for (int k = 0; k < countries.getLength(); k++) {
								if (getUserNode(k, countries, countryName) != null) {
									users.add(getUserNode(k, countries, countryName));
								}
							}
						}
					}
				}
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	

	public List<String> getNodeInXMLFile(NodeList data) {
		List<String> nodeList = new ArrayList<String>();
		for (int i = 0; i < data.getLength(); i++) {
			Element existNode = (Element) data.item(i);
			String nodeName = existNode.getAttribute("name");
			nodeList.add(nodeName);
		}
		return nodeList;
	}

	public Node getNode(NodeList data, String nodeName) {
		for (int i = 0; i < data.getLength(); i++) {
			Element tmpNode = (Element) data.item(i);
			String nameOfExistNode = tmpNode.getAttribute("name");
			if (nameOfExistNode.equals(nodeName)) {
				return (Element) data.item(i);
			}
		}
		return null;
	}

	



	public Node getUser(Document doc, User regisUser) {

		Element user = doc.createElement("user");
		user.appendChild(getUserElements(doc, "first_name", regisUser.getFirstName()));
		user.appendChild(getUserElements(doc, "last_name", regisUser.getLastName()));
		user.appendChild(getUserElements(doc, "address", regisUser.getAddress()));
		user.appendChild(getUserElements(doc, "city", regisUser.getCity()));
		user.appendChild(getUserElements(doc, "email", regisUser.getEmail()));
		user.appendChild(getUserElements(doc, "password", regisUser.getPassword()));

		return user;
	}

	private Node getUserElements(Document doc, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}


	public boolean existUser(List<User> users, User newUser) {
		for (User user : users) {
			if (user.getEmail().equals(newUser.getEmail())) {
				return true;
			}
		}
		return false;
	}

	public boolean addUser(User newUser) {
		List<User> users = this.findAllUsers();
		if (this.existUser(users, newUser)) {
			return false;
		} else {
			users.add(newUser);
			printService.printInXmlFile(users);
			return true;
		}

	}

	public User findUser(String userEmail) {
		List<User> users = this.findAllUsers();
		for (User user : users) {
			if (userEmail.equals(" " + user.getEmail())) {
				return user;
			}
		}
		return null;
	}

	public void deleteUser(User user) {
		List<User> users = this.findAllUsers();
		Iterator<User> usersIterator = users.iterator();
		while (usersIterator.hasNext()) {
			User nextUser = usersIterator.next();
			if (nextUser.equals(user)) {
				usersIterator.remove();
			}
		}

		printService.printInXmlFile(users);
	}

}