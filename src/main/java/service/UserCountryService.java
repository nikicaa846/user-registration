package service;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import model.User;

public class UserCountryService {

	private UserService userService;

	public Node getCountry(Document doc, User user) {
		Element country = null;
		List<String> countries = new ArrayList<String>();
		Element rootElement = (Element) doc.getDocumentElement();
		NodeList data = rootElement.getChildNodes();
		for (int i = 0; i < data.getLength(); i++) {
			NodeList continents = userService.getNodeList(i, data);
			countries = userService.getNodeInXMLFile(continents);
			if (countries.contains(user.getCountry())) {
				country = (Element) userService.getNode(continents, user.getCountry());
				country.appendChild(userService.getUser(doc, user));
			} else {
				country = doc.createElement("country");
				country.setAttribute("name", user.getCountry());
				country.appendChild(userService.getUser(doc, user));
			}
		}
		return country;
	}
}
