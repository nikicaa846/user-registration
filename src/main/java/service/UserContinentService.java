package service;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import model.User;

public class UserContinentService {

	private UserService userService;

	private UserCountryService countryService;

	private String getNameOfContinent(String country) {
		String nameOfContinent = "";
		if ("United States".equals(country) || "Canada".equals(country)) {
			nameOfContinent = "North America";
		} else if ("United Kingdom".equals(country) || "France".equals(country) || "Germany".equals(country)) {
			nameOfContinent = "Europe";
		} else if ("China".equals(country) || "Japan".equals(country)) {
			nameOfContinent = "Asia";
		}
		return nameOfContinent;
	}

	public Node getContinent(Document doc, User user) {
		Element rootElement = (Element) doc.getDocumentElement();
		String nameOfContinent = getNameOfContinent(user.getCountry());
		if (rootElement.hasChildNodes()) {
			NodeList data = rootElement.getChildNodes();
			List<String> continents = userService.getNodeInXMLFile(data);
			return appendContinentWithChild(continents, doc, data, nameOfContinent, user);
		} else {
			return appendContinentWithNoChildren(doc, nameOfContinent, user, rootElement);
		}
	}

	private Node appendContinentWithChild(List<String> continents, Document doc, NodeList data, String nameOfContinent,
			User user) {
		if (continents.contains(nameOfContinent)) {
			Element continent = userService.getNode(data, nameOfContinent);
			return continent.appendChild(countryService.getCountry(doc, user));
		} else {
			Element continent = doc.createElement("continent");
			continent.setAttribute("name", nameOfContinent);
			return continent.appendChild(countryService.getCountry(doc, user));
		}
	}

	private Node appendContinentWithNoChildren(Document doc, String nameOfContinent, User user, Element rootElement) {
		Element continent = doc.createElement("continent");
		continent.setAttribute("name", nameOfContinent);
		rootElement.appendChild(continent);
		Element country = doc.createElement("country");
		country.setAttribute("name", user.getCountry());
		country.appendChild(userService.getUser(doc, user));
		return continent.appendChild(country);
	}

}
