package service;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.User;

public class PrintService {

	private UserContinentService continentService;

	public void printInXmlFile(List<User> users) {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElement("data");
			appendContinent(users, doc, rootElement);
			transform(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void appendContinent(List<User> users, Document doc, Element rootElement) {
		doc.appendChild(rootElement);
		for (User user : users) {
			rootElement.appendChild(continentService.getContinent(doc, user));
		}
	}
	
	private void transform(Document doc) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult file = new StreamResult(new File(UserService.PATH));
		transformer.transform(source, file);
	}
}
