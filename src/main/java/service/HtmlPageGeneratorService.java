package service;

import java.util.List;

import model.User;

public class HtmlPageGeneratorService {

	public String generatedUsersPage(List<User> users) {

		StringBuilder out = new StringBuilder();
		out.append("<html>");
		out.append("<body>");
		out.append("<head>\r\n" + "<meta charset=\"ISO-8859-1\">\r\n");
		out.append("<link rel=\"stylesheet\" href=\"userregistration.css\">\r\n");
		out.append("<title>Registration</title>\r\n");
		out.append("</head>");
		out.append("<h1>User registration</h1>");
		out.append("<br>");
		out.append("<form action=\"/Project/users\" method=\"post\">");
		out.append("<div>\r\n");
		out.append("<label>First name: </label> <input type=\"text\" id=\"fname\" name=\"fname\">\r\n");
		out.append("<br>\r\n");
		out.append("<br> <label>Last name: </label> <input type=\"text\" id=\"lname\"\r\n");
		out.append("name=\"lname\"> <br>\r\n");
		out.append("<br> <label>Address: </label> <input type=\"text\" id=\"address\"\r\n");
		out.append("name=\"address\"> <br>\r\n");
		out.append("<br> <label>City: </label> <input type=\"text\" id=\"city\"\r\n");
		out.append(" name=\"city\"> <br>\r\n");
		out.append("<br> <label for=\"country\">Country: </label> <select\r\n");
		out.append(" name=\"country\" id=\"country\">\r\n");
		out.append("<option value=\"United States\">United States</option>\r\n");
		out.append("<option value=\"Canada\">Canada</option>\r\n");
		out.append("<option value=\"United Kingdom\">United Kingdom</option>\r\n");
		out.append("<option value=\"France\">France</option>\r\n");
		out.append("<option value=\"Germany\">Germany</option>\r\n");
		out.append("<option value=\"China\">China</option>\r\n");
		out.append("<option value=\"Japan\">Japan</option>\r\n");
		out.append("</select> <br>\r\n");
		out.append("<br> <label>E-mail: </label> <input type=\"text\" id=\"email\"\r\n");
		out.append("name=\"email\"> <br>\r\n");
		out.append("<br> <label>Password: </label> <input type=\"text\" id=\"password\"\r\n");
		out.append("name=\"password\"> <br>\r\n");
		out.append("<br>\r\n");
		out.append("<button name=\"registration\" class=\"buttonRegistration\" method=\"post\">REGISTER</button>\r\n");
		out.append("</div>");
		out.append("</form>");
		out.append("<br><br>");
		out.append("<div>");
		out.append("<table class=\"userTable\">");
		out.append("<tr>\r\n");
		out.append("<th>Full name</th>\r\n");
		out.append("<th>Address</th>\r\n");
		out.append("<th>City</th>\r\n");
		out.append("<th>Country</th>\r\n");
		out.append("<th>E-mail</th>\r\n");
		out.append("<th>Action</th>\r\n");
		out.append("</tr>");
		for (User user : users) {
			out.append("<tr>");
			out.append("<td>");
			out.append(user.getFirstName());
			out.append(" ");
			out.append(user.getLastName());
			out.append("</td>");
			out.append("<td>");
			out.append(user.getAddress());
			out.append("</td>");
			out.append("<td>");
			out.append(user.getCity());
			out.append("</td>");
			out.append("<td>");
			out.append(user.getCountry());
			out.append("</td>");
			out.append("<td>");
			out.append(user.getEmail());
			out.append("</td>");
			out.append("<td>");
			out.append("<form action=\"/Project/deleteuser\" method=\"get\">");
			out.append("<button name=\"userEmail\" type=\"submit\" value=\" ");
			out.append(user.getEmail());
			out.append("\" class=\"buttonDelete\">DELETE </button>");
			out.append("</td>");
			out.append("</form>");
			out.append("</tr>");
		}
		out.append("</table>");
		out.append("</div>");
		out.append("</body>");
		out.append("</html>");

		return out.toString();
	}
}
