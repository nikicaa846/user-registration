package model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
	private String firstName;

	private String lastName;

	private String address;

	private String city;

	private String country;

	private String email;

	private String password;
	
	@Override
	public String toString() {
		return "FirstName: " + this.getFirstName() +
			   "\nLastName: " + this.getLastName() + 
			   "\nAddress: " + this.getAddress() + 
			   "\nCity: " + this.getCity() + 
			   "\nCountry: " + this.getCountry() +
			   "\nEmail: " + this.getEmail() + 
			   "\nPassword: " + this.getPassword() ;
	} 

	@Override
	public boolean equals(Object object) {
		User user = (User) object;
		return this.getEmail().equals(user.getEmail());
	}
}
